#%% [markdown]
# Reinforcement Learning with OpenAI Gym
# ---
# This notebook will create and test different reinforcement learning agents and environments.

#%%
## import the needed libraries
import tensorflow as tf
import gym

import numpy as np
import matplotlib.pyplot as plt
import os

#get_ipython().run_line_magic('matplotlib', 'inline')

#%% [markdown]
# Load the Environment
# ---
# Call `gym.make("environment name")` to load a new environment.
# 
# Check out the list of available environments at <https://gym.openai.com/envs/>
# 
# Edit this cell to load different environments!

#%%
# TODO: Load an environment
# make the game environment
env = gym.make("CartPole-v1")


#%%
# TODO: Print observation and action spaces
print(env.observation_space)
print(env.action_space)
#%% [markdown]
# Run an Agent
# ---
# 
# Reset the environment before each run with `env.reset`
# 
# Step forward through the environment to get new observations and rewards over time with `env.step`
# 
# `env.step` takes a parameter for the action to take on this step and returns the following:
# - Observations for this step
# - Rewards earned this step
# - "Done", a boolean value indicating if the game is finished
# - Info - some debug information that some environments provide. 

#%%
# TODO Make a random agent

# establish the number of games to play
games_to_play = 10
for i in range(games_to_play):
    # reset the environment for each game
    obs = env.reset()
    # initialize rewards to 0
    episode_rewards = 0
    # used to track if the current game is done
    done = False

    # loop until done with the game
    while not done:
        #render the environment so we can watch
        # render the environment
        env.render()

        #choose a random action
        action = env.action_space.sample()

        #take a step in the environment with the chosen action, update the episode rewards
        obs, reward, done, info = env.step(action)
        episode_rewards += reward
    #print total episode rewards when done
    print(episode_rewards)



#%% [markdown]
# Policy Gradients
# ---
# The policy gradients algorithm records gameplay over a training period, then runs the results of the actions chosen through a neural network, making successful actions that resulted in a reward more likely, and unsuccessful actions less likely.

#%%
# TODO Build the policy gradient neural network

# define the class for the agent
class Agent:
    def __init__(self, num_actions, state_size):
        # set initializer to the desired initializer (xavier)
        # xavier keeps initial values roughly uniform across layers
        initializer = tf.contrib.layers.xavier_initializer()

        # create the input layer, which will be placeholders with the dimensions [?, shape size]
        self.input_layer = tf.compat.v1.placeholder(dtype = tf.float32, shape=[None, state_size])

        #Neural net starts here
        # create the hidden layers of the neural net
        # use re(ctified)l(inear)un(it) for the activation function
        # have 8 units in the layer
        hidden_layer = tf.layers.dense(self.input_layer, 8, activation=tf.nn.relu, kernel_initializer=initializer)
        hidden_layer_2 = tf.layers.dense(hidden_layer, 8, activation=tf.nn.relu, kernel_initializer=initializer)

        #output of neural net
        out = tf.layers.dense(hidden_layer_2, num_actions, activation=None)

        self.outputs = tf.nn.softmax(out)
        self.choice = tf.argmax(self.outputs, axis=1)

        #training procedure
        self.rewards = tf.placeholder(shape=[None, ], dtype=tf.float32)
        self.actions = tf.placeholder(shape=[None, ], dtype=tf.int32)

        # creates the onehot actions that represent the actions that may be taken in the game
        # will be used for the output neurons, and their probable benefit
        one_hot_actions = tf.one_hot(self.actions, num_actions)

        #cross entropy is used to describe loss in a training step 
        # (difference from expected/desired value between multiple probability distributions)
        # there are only 2 possible actions in this case, so there are 2 probability distributions
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits=out, labels=one_hot_actions)

        # compute the loss by getting the reduced mean of CE * rewards
        self.loss = tf.reduce_mean(cross_entropy * self.rewards)

        self.gradients = tf.gradients(self.loss, tf.trainable_variables())

        #create a placeholder list for gradients
        self.gradients_to_apply = []
        for index, variable in enumerate(tf.trainable_variables()):
            gradient_placeholder = tf.placeholder(tf.float32)
            self.gradients_to_apply.append(gradient_placeholder)

        #create the operation to update gradients with the gradients placeholder
        optimizer = tf.train.AdamOptimizer(learning_rate=1e-2)
        self.update_gradients = optimizer.apply_gradients(zip(self.gradients_to_apply, tf.trainable_variables()))

#%% [markdown]
# Discounting and Normalizing Rewards
# ---
# In order to determine how "successful" a given action is, the policy gradient algorithm evaluates each action based on how many rewards were earned after it was performed in an episode.
# 
# The discount rewards function goes through each time step of an episode and tracks the total rewards earned from each step to the end of the episode.
# 
# For example, if an episode took 10 steps to finish, and the agent earns 1 point of reward every step, the rewards for each frame would be stored as 
# `[10, 9, 8, 7, 6, 5, 4, 3, 2, 1]`
# 
# This allows the agent to credit early actions that didn't lose the game with future success, and later actions (that likely resulted in the end of the game) to get less credit.
# 
# One disadvantage of arranging rewards like this is that early actions didn't necessarily directly contribute to later rewards, so a **discount factor** is applied that scales rewards down over time. A discount factor < 1 means that rewards earned closer to the current time step will be worth more than rewards earned later.
# 
# With our reward example above, if we applied a discount factor of .90, the rewards would be stored as
# `[ 6.5132156   6.12579511  5.6953279   5.217031    4.68559     4.0951      3.439
#   2.71        1.9         1. ]`
# 
# This means that the early actions still get more credit than later actions, but not the full value of the rewards for the entire episode.
# 
# Finally, the rewards are normalized to lower the variance between reward values in longer or shorter episodes.
# 
# You can tweak the discount factor as one of the hyperparameters of your model to find one that fits your task the best!

#%%
# TODO Create the discounted and normalized rewards function
# set the discount rate for the game bot
# may be tweaked to find a discount rate that best fits the model
discount_rate = 0.95

#
def discount_normalize_rewards(rewards):
    discounted_rewards = np.zeros_like(rewards)
    total_rewards = 0

    for i in reversed(range(len(rewards))):
        total_rewards = total_rewards * discount_rate + rewards[i]
        discounted_rewards[i] = total_rewards
    
    discounted_rewards -= np.mean(discounted_rewards)
    discounted_rewards /= np.std(discounted_rewards)

    return discounted_rewards

#%% [markdown]
# Training Procedure
# ---
# The agent will play games and record the history of the episode. At the end of every game, the episode's history will be processed to calculate the **gradients** that the model learned from that episode.
# 
# Every few games the calculated gradients will be applied, updating the model's parameters with the lessons from the games so far.
# 
# While training, you'll keep track of average scores and render the environment occasionally to see your model's progress.

#%%
# TODO Create the training loop
tf.compat.v1.reset_default_graph()

#modify these to match the shape of actions and states in your environment
num_actions = 2
state_size = 4

path = "./cartpole-pg"

training_episodes = 1000
max_steps_per_episode = 10000
episode_batch_size = 5

agent = Agent(num_actions, state_size)
init = tf.global_variables_initializer()
saver = tf.train.Saver(max_to_keep=2)

if not os.path.exists(path):
    os.makedirs(path)

with tf.Session() as sess:
    sess.run(init)

    total_episode_rewards =[]

    #create a buffer of 0'd gradients
    gradient_buffer = sess.run(tf.trainable_variables())
    for index, gradient in enumerate(gradient_buffer):
        gradient_buffer[index] = gradient * 0
    
    #episodes represent individual games in the environment
    for episode in range(training_episodes):
        state = env.reset()

        episode_history = []
        episode_rewards = 0

        #step through the network/game
        for step in range(max_steps_per_episode):
            if episode % 100 == 0:
                env.render()
            #get weights for each action
            action_probabilities = sess.run(agent.outputs, feed_dict={agent.input_layer: [state]})
            action_choice = np.random.choice(range(num_actions), p=action_probabilities[0])

            state_next, rewards, done, _ = env.step(action_choice)
            episode_history.append([state, action_choice, reward, state_next])
            state = state_next

            episode_rewards += reward

            if done or step+1 == max_steps_per_episode:
                total_episode_rewards.append(episode_rewards)
                episode_history = np.array(episode_history)
                episode_history[:,2] = discount_normalize_rewards(episode_history[:,2])

                ep_gradients = sess.run(agent.gradients, feed_dict={agent.input_layer: np.vstack(episode_history[:,0]), 
                                                                    agent.actions: episode_history[:,1], 
                                                                    agent.rewards: episode_history[:,2]})

                #add the gradients to the grad buffer:
                for index, gradient in enumerate(ep_gradients):
                    gradient_buffer[index] += gradient
                
                break
        if episode % episode_batch_size == 0:
            feed_dict_gradients = dict(zip(agent.gradients_to_apply, gradient_buffer))

            sess.run(agent.update_gradients, feed_dict=feed_dict_gradients)

            for index, gradient in enumerate(gradient_buffer):
                gradient_buffer[index] = gradient * 0
            
            if episode % 100 == 0:
                # saver.save(sess, path + "pg-checkpoint", episode)
                saver.save(sess, path + "/pg-checkpoint", global_step=episode)
                print("Average reward / 100 eps: " + str(np.mean(total_episode_rewards[-100:])))



#%% [markdown]
# Testing the Model
# ---
# 
# This cell will run through games choosing actions without the learning process so you can see how your model has learned!

#%%
# TODO Create the testing loop

testing_episodes = 5
with tf.Session() as sess:
    # checkpoint = tf.train.get_checkpoint_state(path)
    # checkpoint = saver.recover_last_checkpoints()
    saver.restore(sess, path)
    # saver.restore(sess, checkpoint)


    for episode in range(testing_episodes):
        state = env.reset()

        episode_rewards = 0

        for step in range(max_steps_per_episode):
            env.render()

            #get action
            action_argmax = sess.run(agent.choice, feed_dict={agent.input_layer: [state]})
            action_choice = action_argmax[0]

            state_next, reward, done, _ = env.step(action_choice)
            state = state_next

            episode_rewards += rewards

            if done or step + 1 == max_steps_per_episode:
                print("Rewards for episode " + str(episode) + ": " + str(episode_rewards))
                break


#%%
# Run to close the environment
env.close()